#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Michael Patron'

"""
    unit test for chained_list 
"""

import unittest
from chained_list import ChainedList
from node import Node


class TestChainedList(unittest.TestCase):
    """ Test case of chained_list """

    def setUp(self):
        """ Initialisation of test """
        self.empty_chained_list = ChainedList()
        node = Node(17)
        self.chained_list = ChainedList(node)

    def test_empty_linked_list(self):
        """ Check if a linked list is empty """
        self.assertIsNone(self.empty_chained_list.first_node)

    def test_none_empty_linked_list(self):
        """ Check if a list with a node is not empty"""
        self.assertIsNotNone(self.chained_list.first_node)

    def test_useless_thing(self):
        """ Check if add and delete of same node consecutively is unchanged """
        first_node = Node(24)
        self.empty_chained_list.insert_node(first_node)

        new_node = Node(35)
        self.empty_chained_list.insert_node(new_node)
        self.empty_chained_list.delete_node(24)
        self.assertTrue(self.empty_chained_list.first_node == first_node)

    def test_stacking(self):
        """ Test if the last node is always the node with the higher value"""

        new_node = Node(35)
        add_node = Node(8)
        add_node2 = Node(7)
        self.chained_list.insert_node(new_node)
        self.chained_list.insert_node(add_node)
        self.chained_list.insert_node(add_node2)

        last_node = None
        iteration = self.chained_list.first_node
        while iteration is not None:

            if iteration.link is None:
                last_node = iteration

            iteration = iteration.link
        self.assertTrue(last_node == new_node)
