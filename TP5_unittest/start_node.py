#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Main execution program
"""

__author__ = 'Michael Patron'

from node import Node
from chained_list import ChainedList

if __name__ == "__main__":

    node = Node(17)
    chained_list = ChainedList(node)

    new_node = Node(8)
    chained_list.insert_node(new_node)

    chained_list.delete_node(8)

    chained_list.__str__()
