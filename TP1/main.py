#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Main execution program
"""

__author__ = 'Michael Patron'

from adn import is_valid, get_valid_adn

if not is_valid("gtka"):
    get_valid_adn()
