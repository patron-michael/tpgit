#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    A library for DNA string analyses
"""

__author__ = 'Michael Patron'


def is_valid(adn_str):
    nb = 0
    for letter in adn_str:
        if letter != "t" and letter != "a" and letter != "g" and letter != "c":
            return False
        else:
            nb += 1
            if nb == len(adn_str):
                return True


def get_valid_adn(prompt='chaîne : '):
    while not is_valid(prompt):
        prompt = ''
        prompt += input("Enter your DNA string:\n")
