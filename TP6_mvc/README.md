README
=====


# Address list

---
Program for conserve contact details

## Table of contents

---
* [How to run program](#How to run program)
* [Technologie](#Technologie)
* [Information](#Information)

## How to run program

---

``python3 main.py``

## Technologie

---
python 3.9.9

## Information

---
Fallow MVC convention design

Structure of script:
- 1 abstract class _view
- 2  view class inherited from _view
    * view_cli, CLI interface
    * view_tkinter, GUI interface realized with tkinter
- 1 controller, controller
- 1 model, with 2 class Person and Ensemble
- main, for execute program

