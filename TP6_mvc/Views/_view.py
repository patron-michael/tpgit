#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Abstract class Views of the project
"""

__author__ = 'Michael Patron'

from tkinter import Button
from tkinter import Label


class MotherView:
    """
    Master view Class
    """

    def __init__(self, controller, type_view):
        """
        Constructor of Master view class
        parameters:
            controller: Controller object
            type_view : String of view name
        """
        self.controller = controller
        self.type_view = type_view
        self.entry = {}
        self.error = {1: ["Quitter", "La personne n'existe pas dans l'annuaire",
                          "Personne introuvable"],
                      2: ["Quitter", "La personne existe déjà dans l'annuaire",
                          "1 personne trouvée"],
                      3: ["Quitter",
                          "Le nom et le prénom sont obligatoire pour supprimer une personne",
                          "Erreur format"]}
        self.entries = ["Nom", "Prenom", "Telephone", "Adresse", "Ville"]

    def choice_response(self, response, windows=None):
        """
        Retrieve the answer of user choice about interface to use and launch the corresponding view
        parameters:
            response: String or tkinter.Button object containing the user choice
            windows: Optional argument, None by default, tkinter.TopLevel object in other case
        """
        if isinstance(response, str):
            answer = response
        elif isinstance(response, Button):
            answer = response["text"]
            windows.destroy()
        else:
            raise Exception("Error in response type")
        self.controller.start_view(answer)

    def interface_choice(self):
        """
        Ask user the type of interface he want to use and lauch choice_response methode
        """
        if self.type_view == "cli":
            answer = input("Affichage en GUI ou CLI ? [GUI/CLI] ?\t")
            self.choice_response(answer)
        elif self.type_view == "tk":
            label = Label(self, text=" Choisissez votre interface")

            self.title("Interface")
            button_gui = Button(self, text="GUI",
                                command=lambda: self.choice_response(button_gui, self))

            button_cli = Button(self, text="CLI",
                                command=lambda: self.choice_response(button_cli, self))

            label.grid(row=0, columnspan=2)
            button_cli.grid(row=1, column=0)
            button_gui.grid(row=1, column=1)

            self.mainloop()
