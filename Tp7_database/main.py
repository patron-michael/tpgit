#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Main execution program
"""

__author__ = 'Olivier Chabrol'

from Controller.controller import Controller

if __name__ == "__main__":
    address_list = Controller()
    address_list.start()
