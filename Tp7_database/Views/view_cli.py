#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CLI class Views of the project
"""

__author__ = 'Michael Patron'

from Views._view import MotherView


class View(MotherView):
    """
    Cli view object heritage from MotherView
    """

    def __init__(self, controller):
        """
        Constructor of Views object
        param:
            controller: controller object
        """
        super().__init__(controller, "cli")
        self.running = True

    def get_value(self, key):
        """
        Return the corresponding value of the field in dictionary
        parameter:
            key: String corresponding key in dictionary
        return:
            value: dictionary value
        """
        return self.entry.get(key)

    def error_message(self, error_code):
        """
        Create message error
        parameter:
            error_code: Int corresponding code error
        return: String error
        """
        for code in self.error.items():
            error_string = "Issues in code error"
            if error_code == code[0]:
                code = code[1]
                error_string = f"Type error: {error_code}\nError: {code[2]}\n\t{code[1]}"

            return print(error_string, end="\n\n")

    def interface_generation(self):
        """
        Main interface who ask which type of functionality launch
        """
        super().entry = {}
        answer = input("Voulez-vous inserer [1], supprimer [2], "
                       "rechercher [3] dans l'annuaire ou quitter [4] ?\t")
        if answer == "1":
            for information in self.entries:
                answer = input(f"{information}: ")
                self.entry[f"{information}"] = answer

            self.controller.button_press_handle("Inserer")

        elif answer == "2":
            lastname = input("Nom: ")
            firstname = input("Prenom: ")
            self.entry["Nom"] = lastname
            self.entry["Prenom"] = firstname

            self.controller.button_press_handle("Effacer")

        elif answer == "3":
            lastname = input("Nom: ")
            firstname = input("Prenom: ")
            self.entry["Nom"] = lastname
            self.entry["Prenom"] = firstname

            self.controller.button_press_handle("Chercher")

        elif answer == "4":
            if self.controller.quite():
                return False
        return True

    @staticmethod
    def information(type_message):
        """
        Create a message for inform the user
        parameter:
            type_message: type of message to create
        return:
            string_ok: [String] with message
        """
        if type_message == "Insert":
            string_ok = "La personne a été ajoutée a l'annuaire"
        elif type_message == "Delete":
            string_ok = "La personne a été supprimer de l'annuaire"
        else:
            raise Exception("Error generation information pop_up")

        return string_ok

    @staticmethod
    def list_view(list_person):
        """
        Display information about corresponding person
        parameter:
            list_person: list of person object
        """
        for person in list_person:
            print("-------Fiche personnelle-------")
            print(person.__str__())
            print("-------------------------------", end="\n\n")

    def quit_confirm(self):
        """
        Ask if user want to quit the program.
        """
        answer = input("Voulez-vous quitter l'annuaire ? [oui/non]\t")
        if answer == "oui":
            self.running = False

    def main(self):
        """
        Generate the main interface display
        """
        print("\n\tAnnuaire")
        while self.running:
            self.interface_generation()
