#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Graphical interface class Views of the project
"""

__author__ = 'Michael Patron and DLAD graphical interface team'

from tkinter import Tk
from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import StringVar
from tkinter import Toplevel
from tkinter.ttk import Treeview
from tkinter.messagebox import askyesno
from Views._view import MotherView


class View(MotherView, Tk):
    """
    Views class, heritage from MotherView
    """

    def __init__(self, controller):
        """
        Views object constructor
        parameter:
            controller: controller objcct
        """
        Tk.__init__(self)
        MotherView.__init__(self, controller, "tk")
        self.widgets_labs = {}
        self.widgets_button = {}
        self.buttons = ["Chercher", "Inserer", "Effacer"]
        # self.modelListFields = []
        # self.fileName = None
        # self.windows = {}
        # self.windows["fenetreResult"] = ...
        # self.windows["fenetreErreur"] = ...
        self.protocol = self.protocol

    def get_value(self, key):
        """"
        Return the corresponding value of the field in dictionary
        parameter:
            key: String corresponding key in dictionary
        return:
            value: dictionary value
        """
        return self.entry[key].get()

    def interface_generation(self):
        """
        Generate Tk windows for main interface who ask which type of functionality launch
        """
        i, j = 0, 0

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i, column=0)

            var = StringVar()
            entry = Entry(self, text=var)
            self.entry[idi] = entry
            entry.grid(row=i, column=1)

            i += 1

        for idi in self.buttons:
            button_w = Button(self, text=idi,
                              command=(lambda button=idi:
                                       self.controller.button_press_handle(button)))
            self.widgets_button[idi] = button_w
            button_w.grid(row=i + 1, column=j)

            j += 1
        return True

    def information(self, type_message):
        """
        Generate  a tkinter.TopLevel object for inform the user
        parameter:
            type_message: type of message to create
        """
        if type_message == "Insert":
            string_ok = "La personne a été ajoutée a l'annuaire"
        elif type_message == "Delete":
            string_ok = "La personne a été supprimer de l'annuaire"
        else:
            raise Exception("Error generation information pop_up")

        pop_up = Toplevel(self)
        text_label = Label(pop_up, text=string_ok)
        pop_up.title("Checked")
        button_ok = Button(pop_up, text="Ok", command=pop_up.destroy)

        text_label.pack()
        button_ok.pack()

    def error_message(self, error_code):
        """
        Generate a tkinter.TopLevel object for display error message
        parameter:
            error_code: Int corresponding code error
        """
        for code in self.error.items():
            if error_code == code[0]:
                # After the key is checked we conserve only the values
                code = code[1]
                pop_up = Toplevel(self)
                label = Label(pop_up, text=code[1])
                label.grid(row=0, column=0)
                pop_up.title(code[2])
                pop_up.grab_set()
                button = Button(pop_up, text=code[0], command=pop_up.destroy)
                button.grid(row=2, column=0)

    def list_view(self, list_person):
        """
        Display information about corresponding person in a tkinter.ttk.TreeView object
        parameter:
            list_person: list of person object
        """
        pop_up = Toplevel(self)
        pop_up.title("Liste Annuaire")

        chart = Treeview(pop_up, columns=("Nom", "Prenom", "Telephone", "Adresse", "Ville"))
        chart.heading("Nom", text="Nom")
        chart.heading("Prenom", text="Prenom")
        chart.heading("Telephone", text="Telephone")
        chart.heading("Adresse", text="Adresse")
        chart.heading("Ville", text="Ville")

        for person in list_person:
            chart.insert("", "end", values=(person.get_nom(), person.get_prenom(),
                                            person.get_telephone(), person.get_adresse(),
                                            person.get_ville()))

        chart['show'] = "headings"
        chart.pack(padx=10, pady=(0, 10))

        button = Button(pop_up, text="Quitter", command=pop_up.destroy)
        button.pack(padx=10, pady=(0, 10))

    def quit_confirm(self):
        """
        Ask if user want to quit the program.
        """
        if askyesno("Quitter", "Voulez-vous vraiment quitter cette application ?"):
            self.destroy()

    def main(self):
        """
        Generate the main interface display
        """
        print("[Views] main")
        self.title("Annuaire")
        self.interface_generation()
        self.mainloop()
