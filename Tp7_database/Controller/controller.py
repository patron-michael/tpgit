#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
    Controller class for address list
"""

__author__ = 'Michael Patron and DLAD team'

import Views
from Views.view_cli import View
from Views.view_tkinter import View
from Model.person import Person
from Model.ensemble import Ensemble


class Controller:
    """
    Controller class object
    """

    def __init__(self):
        """
        Controller constructor
        """
        self.view = View(self)
        self.model = Ensemble()
        # Allow data persistence
        try:
            self.model.load_data()
            print("[Controller][Load_data] Data recover")
        except FileNotFoundError:
            print("[Controller][Load_data] No data to pick")
        # General issus catch
        finally:
            print("Error occurred during data loading")

    def start(self):
        """
        Start the program and launch interface_choice method
        """
        self.view.interface_choice()

    def start_view(self, answer):
        """
        Connect the corresponding Views depending on user choice
        parameter:
            answer: String with user response about interface
        """
        if answer.upper() == "GUI":
            self.view = Views.view_tkinter.View(self)
            # Initialise the closure protocole specific of Tkinter view
            self.view.protocol("WM_DELETE_WINDOW", self.quite)
        elif answer.upper() == "CLI":
            self.view = Views.view_cli.View(self)

        self.view.main()

    def search(self):
        """
        Search a person in the dictionary with information recuperate from interface.
        Launch list_view method
        """
        if self.view.get_value("Prenom"):
            name = f"{self.view.get_value('Prenom').capitalize()}" \
                   f" {self.view.get_value('Nom').capitalize()}"
            person = self.model.search_person(name)
            self.view.list_view(person)
        else:
            name = f"{self.view.get_value('Nom').capitalize()}"
            list_person = self.model.search_person(name)
            self.view.list_view(list_person)

    def delete(self):
        """
        Delete a person in the dictionary with information recuperate from interface.
        Launch delete_person method.
        Generate a message status or error.
        """
        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))

        code_error = self.model.delete_person(person)
        if code_error is not None:
            self.view.error_message(code_error)
        else:
            self.view.information("Delete")

    def quite(self):
        """
        Launch save_data method then quit_confirm method
        """
        # Even the user don't want to close the program data are saved
        self.model.save_data()
        if self.view.quit_confirm():
            print("[Controller][Quit_press] Save data ")

    def insert(self):
        """
        Recuperate information in interface and create person
        Object and launch insert_person methode.
        Generate code error or information status
        """
        person = Person(self.view.get_value("Nom"),
                        self.view.get_value("Prenom"),
                        self.view.get_value("Telephone"),
                        self.view.get_value("Adresse"),
                        self.view.get_value("Ville"))
        code_error = self.model.insert_person(person)
        if code_error is not None:
            self.view.error_message(code_error)
        else:
            self.view.information("Insert")

    def button_press_handle(self, user_choice):
        """
        Launch search() or delete() or insert() in function of user choice
        parameter:
            user_choice: String with user choice
        """
        print("[Controller][button_press_handle] " + user_choice)
        if user_choice == "Chercher":
            self.search()
        elif user_choice == "Effacer":
            self.delete()
        elif user_choice == "Inserer":
            self.insert()
