#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Model class for address list
"""

__author__ = 'Michael Patron and DLAD model team'

from pickle import dump, load
from Model.person import Person


class Ensemble:
    """
    Class Ensemble
    """

    def __init__(self):
        """
        Constructor of Ensemble class
        """
        self.list_person = {}

    def insert_person(self, person):
        """
        Insert a person object in dictionary
        Return error code or None
        parameter:
            person: Person object
        return:
            [int] if error or [None]
        """
        person.set_nom(person.get_nom().capitalize())
        person.set_prenom(person.get_prenom().capitalize())

        # Check if a firstname or lastname in entry
        if len(person.nom) == 0 and len(person.prenom) == 0:
            return 3
        # Check if the key exist in the dictionary
        if isinstance(person, Person) and f"{person.prenom} {person.nom}" not in self.list_person:
            prenom = person.get_prenom()
            nom = person.get_nom()
            self.list_person[f"{prenom} {nom}"] = person
            print(f'Insert: {nom} {prenom}')
            return None

        return 2

    def delete_person(self, person):
        """
        Delete a person object from dictionary
        Return error code or None
        parameter:
            person: Person object
        return:
            [int] if error or [None]
        """
        person.set_nom(person.get_nom().capitalize())
        person.set_prenom(person.get_prenom().capitalize())

        # Check if a firstname or lastname in entry
        if len(person.nom) == 0 or len(person.prenom) == 0:
            return 3

        # Check if the key exist in the dictionary
        if isinstance(person, Person) and f"{person.prenom} {person.nom}" in self.list_person:
            values_to_del = []
            for element in self.list_person.keys():
                if f"{person.prenom} {person.nom}" in element:
                    values_to_del.append(element)
            for to_del in values_to_del:
                del self.list_person[to_del]
                print(f'Deleted: {to_del}')

        if isinstance(person, Person) and f"{person.prenom} {person.nom}" not in self.list_person:
            return 1

        return None

    def search_person(self, name):
        """
        Check if the name existe in the dictionary of
        person and return a list of person corresponding
        parameter:
            name: String compose of firstname or lastname
            or lastname plus firstname
        return:
            names_fetched: [List] of person
        """
        names_fetched = []
        for element in self.list_person.items():
            if name in element[0]:
                names_fetched.append(element[1])
        return names_fetched

    def save_data(self):
        """
        Save the person in binary file when program is closed
        """
        path = "Data/annuaire.data"
        with open(path, "wb") as handler:
            data = {}
            # Conserve person object in dictionary with lastname plus firstname in key
            for person in self.list_person.values():
                data[f"{person.get_nom()} {person.get_prenom()}"] = person
            dump(data, handler)

    def load_data(self):
        """
        Load data from binary file and fill person dictionary
        """
        path = "Data/annuaire.data"
        with open(path, "rb") as handler:
            # Loading from binary file
            loader = load(handler)

            # Recreate all person object
            for element in loader:
                person = loader[element]
                # If the person do not existe yet in the person dictionary we add it
                if isinstance(person, Person) and f"{person.prenom} " \
                                                  f"{person.nom}" not in self.list_person:
                    firstname = person.get_prenom()
                    lastname = person.get_nom()
                    # The key is composed of firstname plus lastname
                    self.list_person[f"{firstname} {lastname}"] = person
                    print(f'Load: {lastname} {firstname}')

    def __str__(self):
        """
        Display information about all person in dictionary
        return:
            print [String] calling __str__ Person object class function
        """
        for person in self.list_person:
            return print(person.__str__())
