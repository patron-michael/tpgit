#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Model class for address list
"""

__author__ = 'Michael Patron and DLAD model team'


class Person:
    """
    Class Person
    """

    def __init__(self, nom, prenom, telephone='', adresse='', ville=''):
        """
        Constructor of Person class
        Parameters:
            nom: String lastname
            prenom: String firstname
            telephone: String of the phone number
            adresse: String of the address
            ville: String of city name
        """
        self.nom = nom
        self.prenom = prenom
        self.telephone = telephone
        self.adresse = adresse
        self.ville = ville

    def get_nom(self):
        """
        Return the lastname
        return:
            nom [String]
        """
        return self.nom

    def set_nom(self, nom):
        """
        Set the lastname
        parameter:
            nom: String lastname of the person
        """
        if isinstance(nom, str):
            self.nom = nom

    def get_prenom(self):
        """
        Return the firstname
        return:
            prenom [String]
        """
        return self.prenom

    def set_prenom(self, prenom):
        """
        Set the firstname
        parameter:
            prenom: String firstname of the person
        """
        if isinstance(prenom, str):
            self.prenom = prenom

    def get_telephone(self):
        """
        Return the phone number
        return:
            telephone: [String]
        """
        return self.telephone

    def get_adresse(self):
        """
        Return address
        return:
            adresse: [String]
        """
        return self.adresse

    def get_ville(self):
        """
        Return city name
        return:
            ville [String]
        """
        return self.ville

    def __str__(self):
        """
        Display information about person
        return:
            print [String] with lastname, firstname, phone number, address, city name
        """
        return f"Nom: {self.get_nom()}\nPrenom: {self.get_prenom()}" \
               f"\nTelephone: {self.get_telephone()}\n" \
               f"Adresse: {self.get_adresse()}\nVille: {self.get_ville()}"
