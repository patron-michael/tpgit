README
======

# Sort chained list object
Program for manipulate an object of type sort chained list.

- Allow creation of chained list.
- Insertion, deletion of node.
- List content display.

## Table of contents
* [How to run program](#How to run program)
* [Technologie](#Technologie)
* [Library](#Library)

## How to run program

```
python3 startNode.py
```

## Technologie

python 3.9.10

## Library

- node

- chained_list
