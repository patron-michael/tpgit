#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    library for Chained list
"""

__author__ = 'Michael Patron'

from node import Node


class ChainedList:
    """
    Chained list Object
    Parameters
    ----------
    nodes : A number or a list of number that we want to transfert in a chained list of Node object
    """

    def __init__(self, nodes):
        self.first_node = None
        if isinstance(nodes, list):
            if self.first_node is None:
                new_node = Node(nodes[0])
                self.first_node = new_node
            for element in nodes[1:]:
                new_node = Node(element)
                self.insert_node(new_node)
        else:
            self.first_node = Node(nodes)

    def insert_node(self, new_node):
        """
        insert a new node in the right position in chaine
        Parameters
        ----------
        new_node : node to insert
        """
        status = f"Error in the insertion of {new_node.data}"
        previous = self.first_node

        if previous == new_node:
            status = "The noeud already existe in chained list"
            return print(status)

        if str(new_node.data) <= str(self.first_node.data):
            tmp = self.first_node
            self.first_node = new_node
            self.first_node.link = tmp
            status = f"Successfully insert of {new_node.data} in chained list"

        else:
            iteration = self.first_node
            while iteration is not None:

                if iteration == new_node:
                    status = "The noeud already existe in chained list"
                    return print(status)

                if str(iteration.data) > str(new_node.data) >= str(previous.data):
                    new_node.link = iteration
                    previous.link = new_node
                    status = f"Successfully insert of {new_node.data} in chained list"

                if iteration.link is None and str(new_node.data) > str(iteration.data):
                    iteration.link = new_node
                    status = f"Successfully insert of {new_node.data} in chained list"

                previous = iteration
                iteration = iteration.link

        return print(status)

    def delete_node(self, data):
        """
        delete all node(s) value == data
        Parameters
        ----------
        data : searched data to delete
        """
        status = f"Error for delete nodes with value {data}"
        iteration = self.first_node
        previous = None

        while iteration is not None:
            if str(iteration.data) == str(data):
                iteration = iteration.link

                if previous is None:
                    previous = iteration.link
                else:
                    previous.link = iteration

                status = f"Successfully delete nodes with value {data}"
            else:
                previous = iteration
                iteration = iteration.link

        return print(status)

    def __str__(self):
        """
        Display chained list in string format
        Returns
        -------
        String
        """
        string = ""
        if self.first_node is None:
            string += "Any element in the chained list"
        else:
            iteration = self.first_node
            while iteration:
                if iteration == self.first_node:
                    string += f" {iteration.data} "
                if iteration.link is not None:
                    string += f"-> {iteration.link.data} "

                else:
                    string += f"-> {iteration.link}"
                iteration = iteration.link
        return print(string)
