#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Library for create node
"""

__author__ = 'Olivier Chabrol'


class Node:
    """
        class object for manipulate node
    """
    def __init__(self, param_data):
        self.data = param_data
        self.link = None

    def __str__(self):
        """

        Returns
        -------
            display node list
        """
        list_node = []
        node = self
        while node.link is not None:
            list_node.append(node.data)
            node = node.link
        list_node.append(node.data)
        return str(list_node)
