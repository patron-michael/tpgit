#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Main execution program
"""

__author__ = 'Michael Patron'

from node import Node
from chained_list import ChainedList

if __name__ == "__main__":

    chained_list = ChainedList([7, 'a', 7, 8, 1, 2, 3, 4, 5,
                                6, 7, 8, 'c', 9, 9, 8, 7, 6, 5, 4, 3, 2, 1])
    n12 = Node(-70)

    chained_list.insert_node(n12)
    chained_list.insert_node(n12)

    chained_list.delete_node(8)
    chained_list.delete_node(-70)
    chained_list.delete_node(2)

    chained_list.__str__()

    chained_list.delete_node('a')
