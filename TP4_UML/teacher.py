#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from academic_person import AcademicPerson
# from department import Department
# from teaching import Teaching

"""
    Teacher class object
"""


class Teacher(AcademicPerson):
    """
        Teacher object
    """

    # def __init__(self, last_name, first_name, email, phone_number, hired, salary, departments=[], teaching="",
     #            responsible=False, students=[]):
    def __init__(self, last_name, first_name, email, phone_number, hired, salary,
                     responsible=False, students=[]):
        """
        Constructor of Teacher class, use AcademicPerson abstract class
        Parameters:
            last_name: last name string
            first_name: first name string
            email: email string
            phone_number: phone number int or string
            hired: hire date string
            salary: salary float
            departments: Department object list
            teaching: Teaching object
            responsible: This teacher is a responsible Bool
            students: Student object list
        """
        AcademicPerson.__init__(self, last_name, first_name, email, phone_number, hired)
        # Salary is obligatory be can be initialised at 0
        # We allow to have empty list of departments and/or students
        if isinstance(float(salary), float):
            self.salary = salary
        else:
            raise Exception("Error in salary input")
        # if isinstance(teaching, Teaching):
        #     self.teaching = teaching
        # elif teaching is None:
        #     self.teaching = ""
        # else:
        #     raise Exception("Impossible to link the teaching with this teacher")
        if isinstance(responsible, bool):
            self.responsable = responsible
        else:
            raise Exception("Impossible to change the responsibility of this teacher")
        # if isinstance(departments, list):
        #     self.departments = departments
        # else:
        #     raise Exception("Invalid list of department")
        if isinstance(students, list):
            self.students = students
        else:
            raise Exception("Invalid list of student")

    # def get_teaching(self):
    #     """
    #     Return Teaching object
    #     """
    #     return self.teaching
    #
    # def set_teaching(self, teaching):
    #     """
    #     Modify teaching
    #     Parameter:
    #         teaching: New Teaching object
    #     """
    #     if isinstance(teaching, Teaching):
    #         self.teaching = teaching
    #     else:
    #         raise Exception("Impossible to link the teaching with this teacher")

    def get_salary(self):
        """
        Return salary
        """
        return self.salary

    def set_salary(self, salary):
        """
        Modify salary
        Parameter:
            salary: New salary float
        """
        if isinstance(salary, float):
            self.salary = salary
        else:
            raise Exception("Error in salary input")

    def is_responsible(self):
        """
        Return responsible boolean
        """
        # Can be modified in Teacher class
        return self.responsable

    # def get_departments(self):
    #     """
    #     Return Department object list
    #     """
    #     return self.departments
    #
    # def add_department(self, department):
    #     """
    #     Add department object in Department object list
    #     Parameter:
    #         department: Department object
    #     """
    #     if isinstance(department, Department) and department not in self.departments:
    #         self.departments.append(department)
    #     else:
    #         raise Exception("Impossible to add the department")
    #
    # def delete_department(self, department):
    #     """
    #     delete department object from Department object list
    #     Parameter:
    #         department: Department object
    #     """
    #     if isinstance(department, Department) and department in self.departments:
    #         self.departments.append(department)
    #     else:
    #         raise Exception("Impossible to delete this department")

    def __str__(self):
        """
        Display Teacher information
        """
        return print(f"{self.get_complete_name()}\nEmail: {self.email}\nPhone: {self.phone_number}\n"
                     f"Hired date: {self.hired}\nSalary: {self.salary}\nTeaching: {self.teaching}\n"
                     f"Responsible: {self.is_responsible()}")
