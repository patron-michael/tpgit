#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from person import Person

"""
    AcademicPerson class object
"""


class AcademicPerson(Person):
    """
        AcademicPerson object
    """

    def __init__(self, last_name, first_name, email, phone_number, hired):
        """
        Constructor of AcademicPerson object, call of Person class object
        Parameters:
            last_name: last name string
            first_name: first name string
            email: email string
            phone_number: phone number int or string
            hired: hire date string
        """
        # Use of abstract class Person
        Person.__init__(self, last_name, first_name)
        if isinstance(email, str) and len(email.replace(" ", "")) > 0:
            self.email = email
        else:
            raise Exception("Invalid email")
        if isinstance(phone_number, int):
            phone_number = str(phone_number)
            self.phone_number = phone_number
        elif isinstance(phone_number, str) and len(phone_number.replace(" ", "")) > 0:
            self.phone_number = phone_number
        else:
            raise Exception("Invalid phone number")
        if isinstance(hired, str) and len(hired.replace(" ", "")) > 0:
            self.hired = hired
        else:
            raise Exception("Invalid date format. Must be a string")

    def get_person_card(self):
        """
        Return list information
        """
        information = {"complet name": self.get_complete_name(), "Email": self.email, "Hired": self.hired,
                       "Phone": self.phone_number, "Last name": self.get_last_name(),
                       "First name": self.get_first_name()}
        return information

    def get_email(self):
        """
        Return email string
        """
        return self.email

    def set_email(self, email):
        """
        Modify email string
        Parameters:
            email: New email string
        """
        if isinstance(email, str) and len(email.replace(" ", "")) > 0:
            self.email = email
        else:
            raise Exception("Invalid email")

    def get_phone_number(self):
        """
        Return phone number string
        """
        return self.phone_number

    def set_phone_number(self, phone_number):
        """
        Modify phone number
        Parameter:
            phone_number: New phone number string or int
        """
        if isinstance(phone_number, int):
            phone_number = str(phone_number)
            self.phone_number = phone_number
        elif isinstance(phone_number, str) and len(phone_number.replace(" ", "")) > 0:
            self.phone_number = phone_number
        else:
            raise Exception("Invalid phone number")

    def get_date_hired(self):
        """
        Return hired date
        """
        return self.hired

    def set_date_hired(self, hired):
        """
        Modify hired date
        Parameter:
            hired: New hired date
        """
        if isinstance(hired, str) and len(hired.replace(" ", "")) > 0:
            self.hired = hired
        else:
            raise Exception("Invalid date format. Must be a string")

    def __str__(self):
        """
        Display AcademicPerson information
        """
        return print(f"{self.get_complete_name()}\nEmail: {self.email}\nPhone: {self.phone_number}\n"
                     f"Hired date: {self.hired}")
