#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

"""
    Person object class
"""


class Person:
    """
    Person class
    """

    def __init__(self, last_name, first_name):
        """
        Constructor of abstract class Person
        Parameters:
            last_name: last name string
            first_name: first name string
        """
        if isinstance(last_name, str) and len(last_name.replace(" ", "")) > 0:
            self.last_name = last_name
        else:
            raise Exception("Invalid last name")
        if isinstance(first_name, str) and len(first_name.replace(" ", "")) > 0:
            self.first_name = first_name
        else:
            raise Exception("Invalid first name")

    def get_last_name(self):
        """
        Return last name string
        """
        return self.last_name

    def set_last_name(self, last_name):
        """
        Modify last name string
        Parameter:
            last_name: New last name
        """
        if isinstance(last_name, str) and len(last_name.replace(" ", "")) > 0:
            self.last_name = last_name
        else:
            raise Exception("Invalid last name")

    def get_first_name(self):
        """
        Return fist name string
        """
        return self.first_name

    def set_first_name(self, first_name):
        """
        Modify fist name string
        Parameter:
            first_name: New first name string
        """
        if isinstance(first_name, str) and len(first_name.replace(" ", "")) > 0:
            self.first_name = first_name
        else:
            raise Exception("Invalid first name")

    def get_complete_name(self):
        """
        Return complete name
        """
        return f"{self.first_name} {self.last_name}"
