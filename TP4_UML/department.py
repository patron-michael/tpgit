#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from responsible_teacher import ResponsibleTeacher
from teacher import Teacher
from teaching import Teaching

"""
    Department class object
"""


class Department:
    """
    ------
    Department object
    ------
    """

    def __init__(self, domain, responsible, teachers=[], teachings=[]):
        """
        Constructor of Department object
        Parameters:
            domain: The name of the department
            teachers: List of Teacher object
            responsible: ResponsibleTeacher object
            teachings: List of Teaching object
        """
        # We allow the creation of Department object with empty teachers list and/or empty teachings list
        if isinstance(domain, str) and len(domain.replace(" ", "")) >= 4:
            self.domain = domain
        elif isinstance(domain, str) and len(domain.replace(" ", "")) < 4:
            raise Exception("Domain name can contain only space")
        else:
            raise Exception("Invalid domain string")
        if isinstance(teachers, list):
            self.teachers = teachers
        else:
            raise Exception("Invalid list of teachers")
        if isinstance(responsible, ResponsibleTeacher) and responsible.is_responsible() is True:
            self.responsible_teacher = responsible
        else:
            raise Exception("Invalid responsible. Check if the Teacher is a responsible teacher")
        if isinstance(teachings, list):
            self.teachings = teachings
        else:
            raise Exception("Teachings list is invalid")

    def get_mean(self):
        """
        Return a global mean for each teaching
        """
        pass

    def set_domain(self, domain):
        """
        Modify the domain name string
        parameter:
            domain: New domain name string
        """
        if isinstance(domain, str) and len(domain.replace(" ", "")) > 4:
            self.domain = domain
        elif isinstance(domain, str) and len(domain.replace(" ", "")) <= 4:
            raise Exception("Domain name can contain only space")
        else:
            raise Exception("Invalid domain string")

    def get_domain(self):
        """
        Return domain name string
        """
        return self.domain

    def get_teachers(self):
        """
        Return a list of Teacher object
        """
        return self.teachers

    def add_teacher(self, teacher):
        """
        Add a Teacher object in teachers list
        Parameter:
            teacher: Teacher object
        """
        if isinstance(teacher, Teacher):
            self.teachers.append(Teacher)
        else:
            raise Exception("Impossible to add this teacher to the list")

    def delete_teacher(self, teacher):
        """
        delete a Teacher object from teachers list
        Parameter:
            teacher: Teacher object
        """
        if isinstance(teacher, Teacher) and teacher in self.teachers:
            self.teachers.remove(teacher)
        else:
            raise Exception("Impossible to remove this teacher of the list")

    def add_teaching(self, teaching):
        """
        Add a Teaching object to teachings list
        Parameter:
            teaching: Teaching object
        """
        if isinstance(teaching, Teaching) and teaching not in self.teachings:
            self.teachers.append(teaching)
        else:
            raise Exception("Impossible to add this teaching")

    def get_teachings(self):
        """
        Return Teaching object list
        """
        return self.teachings

    def get_responsible(self):
        """
        return ResponsibleTeacher object
        """
        return self.responsible_teacher

    def set_responsible(self, responsible):
        if isinstance(responsible, ResponsibleTeacher) and ResponsibleTeacher.is_responsible() is True:
            self.responsible_teacher = responsible
        else:
            raise Exception("Invalid responsible. Check if the Teacher is a responsible teacher")

    def __str__(self):
        """
        Display information about department
        """
        team_string = ""
        teaching_string = ""
        for member in self.teachers:
            team_string += f"-{member.get_name()}\n"
        for teach in self.teachings:
            teaching_string += f"-{teach.get_title}\n"
        if len(self.teachers) != 0 and len(self.teachings) != 0:
            return print(f" {self.domain} manage by {self.responsible_teacher} is compose by an pedagogic "
                         f"team: {team_string}. All teachings: {teaching_string}")
        elif len(self.teachers) != 0 and len(self.teachings) > 0:
            return print(f" {self.domain} manage by {self.responsible_teacher}. All teachings: {teaching_string}")
        elif len(self.teachers) > 0 and len(self.teachings) != 0:
            return print(f" {self.domain} manage by {self.responsible_teacher} is compose by an pedagogic "
                         f"team: {team_string}.")
        else:
            raise Exception("Error occurred for display department information")
