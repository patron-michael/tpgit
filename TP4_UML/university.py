#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

"""
    University class object
"""

from classroom import Classroom
from department import Department


class University:
    """
    ----------
    University object
    ----------
    """

    def __init__(self, class_rooms, name, departments=[], web_site=""):
        """
        Constructor of University object
        Parameters:
            web_site : Web domain name string of the university
            class_rooms : List of Class_room object
            departments : List of Departments object
            name: name string of university
        """
        # We allow the creation of University object with empty class_rooms list and/or empty departments list and/or
        # empty web_site string
        if isinstance(web_site, str):
            self.web_site = web_site
        else:
            raise Exception("Website address must be a valid string, not empty")
        if isinstance(name, str) and name != "":
            self.name = name
        else:
            raise Exception("Name must be a string, not empty")
        if isinstance(class_rooms, list):
            self.class_rooms = class_rooms
        else:
            raise Exception("ClassRooms must be a list")
        if isinstance(departments, list):
            self.departments = departments
        else:
            raise Exception("Departments must be a list")

    def get_website(self):
        """
        Return the website domain
        """
        return self.web_site

    def set_website(self, web_site):
        """
        Modify the website domain
        Parameter:
            web_site: The new website domain string
        """
        if isinstance(web_site, str) and web_site != "":
            self.web_site = web_site
        else:
            raise Exception("Website address must be a valid string, not empty")

    def get_name(self):
        """
        Return the name of university
        """
        return self.name

    def set_name(self, name):
        """
        Modify the name of university
        Parameter:
            name: The new name string
        """
        if isinstance(name, str) and name != "":
            self.name = name
        else:
            raise Exception("Name must be a string, not empty")

    def get_classrooms(self):
        """
        Return a list of Classroom
        """
        return self.class_rooms

    def add_classroom(self, classroom):
        """
        Add a Classroom object to the list of classrooms
        Parameters:
            classroom: Classroom object
        """
        if isinstance(classroom, Classroom) and classroom not in self.class_rooms:
            self.class_rooms.append(classroom)
        else:
            raise Exception("Invalid classroom, Impossible ro ")

    def delete_classroom(self, classroom):
        """
        Delete a Classroom object from the classrooms list
        Parameter:
            classroom: Classroom object
        """
        if isinstance(classroom, Classroom) and classroom in self.class_rooms:
            self.class_rooms.remove(classroom)
        else:
            raise Exception("Invalid classroom")

    def get_departments(self):
        """
        Return the list of Department object
        """
        return self.departments

    def add_department(self, department):
        """
        Add a Department object to departments list
        Parameter:
            department: Department object
        """
        if isinstance(department, Department) and department not in self.departments:
            self.departments.append(department)
        else:
            raise Exception("Impossible to add the department")

    def delete_department(self, department):
        """
        Delete a Department object of departments list
        Parameter:
            department: Department object
        """
        if isinstance(department, Department) and department in self.departments:
            self.departments.remove(department)
        else:
            raise Exception("Impossible to remove the department of the list")

    def __str__(self):
        """
        Display name, number of departments, website of University
        Return:
             String
        """
        department_string = ""
        for department in self.departments:
            department_string += f"-{department}\n"

        if len(self.departments) != 0:
            return print(f"{self.name} university dispose of {len(self.departments)} departments\n{department_string}. "
                         f"Find use with our website {self.web_site}")
        elif len(self.departments) == 0:
            return print(f"{self.name} university dispose of {len(self.departments)} department. "
                         f"Find us with our website {self.web_site}")
        else:
            raise Exception("An error are occurred for display university informations")
