#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from note import Note
# from student import Student
from teacher import Teacher

"""
    Teaching class object
"""


class Teaching:
    """
        Teaching object
    """

    # def __init__(self, title, description="", notes=[], teachers=[], students=[]):
    def __init__(self, title, description="", notes=[], teachers=[]):
        """
        Constructor of Teaching object
        Parameters:
            title: title string
            description: description string
            notes: Note object list
            teachers: Teacher object list
            students: Student object list
        """
        # We allow to have empty list of notes, teachers and students
        # We allow to have empty description
        # Title must have at least 2 characters
        if isinstance(title, str) and len(title) > 2:
            self.title = title
        else:
            raise Exception("Invalid title")
        if isinstance(description, str):
            self.description = description
        else:
            raise Exception("Invalid description")
        if isinstance(notes, list):
            self.notes = notes
        else:
            raise Exception("Invalid list of note")
        if isinstance(teachers, list):
            self.teachers = teachers
        else:
            raise Exception("Invalid teacher list")
        # if isinstance(students, list):
        #     self.students = students
        # else:
        #     raise Exception("Invalid student list")

    def get_notes(self):
        """
        Return Note object list
        """
        return self.notes

    def add_note(self, note):
        """
        Add Note object in Notes object list
        Parameter:
            note: Note object
        """
        if isinstance(note, Note) and note not in self.notes:
            self.notes.append(note)
        else:
            raise Exception("Impossible to add this note")

    def delete_note(self, note):
        """
        Delete Note object from Note object list
        Parameter:
            note: Note object
        """
        if isinstance(note, Note) and note in self.notes:
            self.notes.remove(note)
        else:
            raise Exception("Impossible to remove this note")

    def get_mean(self):
        """
        Return mean float
        """
        cum_note = 0
        for note in self.notes:
            cum_note += note.get_note()
        mean = cum_note/len(self.notes)
        return mean

    def get_title(self):
        """
        Return title string
        """
        return self.title

    def set_title(self, title):
        """
        Modify title attribut
        Parameter:
            title: title string
        """
        if isinstance(title, str) and len(title) > 2:
            self.title = title
        else:
            raise Exception("Invalid title")

    def get_description(self):
        """
        Return description string
        """
        return self.description

    def set_description(self, description):
        """
        Modify description string attribut
        Parameter:
            description: description string
        """
        if isinstance(description, str):
            self.description = description
        else:
            raise Exception("Invalid description")

    # def get_students(self):
    #     """
    #     Return Student object list
    #     """
    #     return self.students
    #
    # def add_student(self, student):
    #     """
    #     Add Student object to Student object list
    #     Parameter:
    #         student: Student object
    #     """
    #     if isinstance(student, Student) and student not in self.students:
    #         self.students.append(student)
    #     else:
    #         raise Exception("Impossible to add this student")
    #
    # def delete_student(self, student):
    #     """
    #     Delete Student object from Student object list
    #     Parameter:
    #         student: Student object
    #     """
    #     if isinstance(student, Student) and student in self.students:
    #         self.students.remove(student)
    #     else:
    #         raise Exception("Impossible to remove this student")

    def get_teachers(self):
        """
        Return Teacher object list
        """
        return self.teachers

    def add_teacher(self, teacher):
        """
        Add Teacher object to Teacher object list
        Parameter:
            teacher: Teacher object
        """
        if isinstance(teacher, Teacher) and teacher not in self.teachers:
            self.teachers.append(teacher)
        else:
            raise Exception("Impossible to add this teacher")

    def delete_teacher(self, teacher):
        """
        Delete Teacher object from Teacher object list
        Parameter:
            teacher: Teacher object
        """
        if isinstance(teacher, Teacher) and teacher in self.teachers:
            self.teachers.remove(teacher)
        else:
            raise Exception("Impossible to remove this teacher")
