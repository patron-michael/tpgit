#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

# from student import Student
# from teaching import Teaching

"""
    Note class object
"""


class Note:
    """
        Note object
    """

    # def __init__(self, student, teaching, note=0):
    def __init__(self, note=0):

        """
        Construct of Note object
        Parameter:
            note: float
        """
        # By default note=0
        if isinstance(float(note), float):
            self.note = note
        else:
            raise Exception("Invalid note")
        # if isinstance(student, Student):
        #     self.student = student
        # else:
        #     raise Exception("Cannot set this student")
        # if isinstance(teaching, Teaching):
        #     self.teaching = teaching
        # else:
        #     raise Exception("Cannot set this teaching")

    def get_note(self):
        """
        Return note float
        """
        return self.note

    def set_note(self, note):
        """
        Modify note attribut
        Parameter:
            note: float
        """
        if isinstance(float(note), float):
            self.note = note
        else:
            raise Exception("Invalid note")

    # def get_teaching(self):
    #     """
    #     Return Teaching object
    #     """
    #     return self.teaching
    #
    # def set_teaching(self, teaching):
    #     """
    #     Modify teaching attribut
    #     Parameter:
    #         teaching: Teaching object
    #     """
    #     if isinstance(teaching, Teaching):
    #         self.teaching = teaching
    #     else:
    #         raise Exception("Cannot set this teaching")
    #
    # def get_student(self):
    #     """
    #     Return student object
    #     """
    #     return self.student
    #
    # def set_student(self, student):
    #     """
    #     Modify student attribut
    #     Parameter:
    #         student: Student object
    #     """
    #     if isinstance(student, Student):
    #         self.student = student
    #     else:
    #         raise Exception("Cannot set this student")
