#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from classroom import Classroom
from note import Note
from responsible_teacher import ResponsibleTeacher
from student import Student
from teacher import Teacher
from teaching import Teaching
from university import University
from department import Department

if __name__ == "__main__":
    note_1 = Note(10)
    note_2 = Note(20)
    note_3 = Note(7)
    note_4 = Note(58)

    responsible_sleeping = ResponsibleTeacher("blo", "bloblo", "blo@bloby", "01248598", "10-05-1862", 1300, "dodo")
    sleep_teacher = Teacher("bla", "blabla", "sleep@sleepy", "01236548", "10-07-2015", 1200)
    tete = Teacher("blo", "bloblo", "sr@ge", "01544548", "10-11-2017", 1200)

    sleeping = Teaching("Sleeping courses", "For sleep in day", teachers=[sleep_teacher], notes=[note_1,
                                                                                                 note_3])
    blabla = Teaching("blabla courses", "", teachers=[sleep_teacher])
    bloblo = Teaching("bloblo courses", "", teachers=[sleep_teacher, tete], notes=[note_4])

    classroom_014 = Classroom("g32", 17, [sleeping])
    student_1 = Student("reg", "fsc", "srfg@egr", "156455896", "10-01-2011")
    dodo = Department("dodo", responsible=responsible_sleeping, teachers=[sleep_teacher], teachings=[sleeping])
    myUniversity = University(web_site="uni-amu.fr", class_rooms=[classroom_014], departments=[dodo],
                              name="Aix_Marseille")

    student_1.add_note(note_1)
    student_1.add_note(note_2)

    student_1.add_teaching(sleeping)
    student_1.add_teaching(blabla)
    student_1.add_teaching(bloblo)

    print(student_1.get_mean())
    print(sleeping.get_mean())

    print(student_1.get_absent_mean())

    print(student_1.get_person_card())
