#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from teacher import Teacher

"""
    ResponsibleTeacher class object
"""


class ResponsibleTeacher(Teacher):
    pass
    """
        ResponsibleTeacher object
    """

    def __init__(self, last_name, first_name, email, phone_number, hired, salary, domain,
                 responsible=True, students=[]):
        """
        Constructor of ResponsibleTeacher class
        Parameters:
            last_name: Last name string
            first_name: First name string
            email: Email string
            phone_number: Phone number int or string
            hired: Hire date string
            salary: Salary float
            responsible: This teacher is a responsible Bool
            domain: Name of department
        """
        Teacher.__init__(self, last_name, first_name, email, phone_number, hired, salary,
                         responsible, students)
        if isinstance(domain, str) and len(domain.replace(" ", "")) > 0:
            self.domain = domain
        else:
            raise Exception("Invalid domain string")

    def get_domain(self):
        """
        return domain string
        """
        return self.domain

    def set_domain(self, domain):
        """
        Modify domain
        Parameter:
             domain: New name of department
        """
        if isinstance(domain, str) and len(domain.replace(" ", "")) > 0:
            self.domain = domain
        else:
            raise Exception("Invalid domain string")

    def __str__(self):
        """
        Display ResponsibleTeacher information
        """
        return print(f"{self.get_complete_name()}\nDomain: {self.domain}\nEmail: {self.email}\n"
                     f"Phone: {self.phone_number}\nHired date: {self.hired}\nSalary: {self.salary}\n"
                     f"Teaching: {self.teaching}\nResponsible: {self.is_responsible()}")
