#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from academic_person import AcademicPerson
from note import Note
from teaching import Teaching

"""
    Student class object
"""


class Student(AcademicPerson):
    """
        Student object
    """

    # def __init__(self, last_name, first_name, email, phone_number, hired, teachings=[], notes=[], teachers=[]):
    def __init__(self, last_name, first_name, email, phone_number, hired, teachings=[], notes=[], teachers=[]):

        """
        Constructor of Student object
        Parameters:
            last_name: last name string
            first_name: first name string
            email: email string
            phone_number: phone number int or string
            hired: hire date string
            teachers: Teacher object list
            teachings: Teaching objet list
            notes: note float list
        """
        # We allow to have empty list of teachings, notes and teachers
        AcademicPerson.__init__(self, last_name, first_name, email, phone_number, hired)
        if isinstance(teachings, list):
            self.teachings = teachings
        else:
            raise Exception("Invalid list of teaching")
        if isinstance(notes, list):
            self.notes = notes
        else:
            raise Exception("Invalid list of notes")
        if isinstance(teachers, list):
            self.teachers = teachers
        else:
            raise Exception("Invalid list of teachers")

    def get_notes(self):
        """
        Return Notes object list
        """
        return self.notes

    def add_note(self, note):
        """
        Add a Note object in notes list
        Parameter:
            note: Note object
        """
        if isinstance(note, Note) and note not in self.notes:
            self.notes.append(note)
        else:
            raise Exception("Impossible to add this note")

    def delete_note(self, note):
        """
        Delete Note object from notes list
        Parameter:
            note: Note object
        """
        if isinstance(note, Note) and note in self.notes:
            self.notes.remove(note)
        else:
            raise Exception("Impossible to delete this note")

    def get_teachings(self):
        """
        Return Teaching object list
        """
        return self.teachings

    def add_teaching(self, teaching):
        """
        Add a Teaching object to teachings list
        """
        if isinstance(teaching, Teaching) and teaching not in self.teachings:
            self.teachings.append(teaching)
        else:
            raise Exception("Impossible to add teaching to teachings list")

    def delete_teaching(self, teaching):
        """
        Delete Teaching object from teachings list
        """
        if isinstance(teaching, Teaching) and teaching in self.teachings:
            self.teachings.remove(teaching)
        else:
            raise Exception("Cannot delete this teaching from teachings list")

    def get_mean(self):
        """
        return mean of student notes
        """
        cum_note = 0
        for note in self.notes:
            cum_note += note.get_note()
        mean = cum_note/len(self.notes)
        return mean

    def get_absent_mean(self):
        """
        Return Teaching title list where Student have no note
        """
        absent_note = []
        for teach in self.teachings:
            if not any(note in self.notes for note in teach.get_notes()) and teach.get_title() not in absent_note:
                absent_note.append(teach.get_title())
        return absent_note
