#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Michael Patron"

from teaching import Teaching

"""
    Classroom object class
"""


class Classroom:
    """
        Classroom object
    """

    def __init__(self, identifier, number_place=0, teachings=[]):
        """
        Constructor of Classroom object
        Parameters:
            identifier: Identification string of the classroom
            teachings: List of Teachings object
            number_place: Number of place in the room
        """
        # We allow to have empty list of teachings
        # Classroom can have any place
        if isinstance(identifier, str) and len(identifier) > 0:
            self.identifier = identifier
        else:
            raise Exception("Identifier invalid")
        if isinstance(teachings, list):
            self.teachings = teachings
        else:
            raise Exception("Invalid list of teaching")
        if isinstance(number_place, int):
            self.number_place = number_place
        else:
            raise Exception("Number of place invalid.")

    def get_identifier(self):
        """
        Return identifier of classroom
        """
        return self.identifier

    def set_identifier(self, identifier):
        """
        Modify identifier
        Parameter:
            identifier: New identifier string
        """
        if isinstance(identifier, str) and len(identifier) > 0:
            self.identifier = identifier
        else:
            raise Exception("Identifier invalid")

    def get_teaching(self):
        """
        Return list Teaching object
        """
        return self.teachings

    def add_teaching(self, teaching):
        """
        Add a teaching object in teaching list
        Parameter:
            teaching: Teaching object
        """
        if isinstance(teaching, Teaching) and teaching not in self.teachings:
            self.teachings.append(teaching)
        else:
            raise Exception("Cannot add this teaching")

    def delete_teaching(self, teaching):
        """
        Delete a teaching from teaching list
        Parameter:
            teaching: Teaching object
        """
        if isinstance(teaching, Teaching) and teaching in self.teachings:
            self.teachings.remove(teaching)
        else:
            raise Exception("Cannot delete this teaching")

    def get_number_place(self):
        """
        return number of place
        """
        return self.number_place

    def set_number_place(self, number_place):
        if isinstance(number_place, int):
            self.number_place = number_place
        else:
            raise Exception("Number of place invalid.")

    def __str__(self):
        """
        Display information about classroom
        """
        teachings_string = ""
        for teach in self.teachings:
            teachings_string += f"-{teach.get_title()}"
        return print(f"Classroom id: {self.identifier}, number of place: {self.number_place}. Teachings in this class"
                     f" {teachings_string}")
