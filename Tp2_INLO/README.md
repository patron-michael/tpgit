README
======

# Fasta contents verification

Program for check the validity of dna in fasta file

## Table of contents
* [How to run program](#How to run program)
* [Technologie](#Technologie)
* [Library](#Library)

## How to run program

`python3 main.py -i fasta_file`

## Technologie

python 3.9.9

## Library

- adn
  - is_valid()

- fasta
  - fasta_check()
  - trimming_fasta()

- select_fasta_parser
  - create_parser()
  - main()
  