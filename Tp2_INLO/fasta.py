#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    A library for Fasta analyses
"""

__author__ = 'Michael Patron'

from adn import is_valid


def fasta_check(fasta):
    """ Check the sequence of fasta file """
    error_string = ""
    reformatted_list = trimming_list(fasta, ["\n", "", None])
    for string in reformatted_list:

        if "\n" in string:
            string = string.replace("\n", "")

        if ">" in string:
            print(string)
            seqence = string
        elif is_valid(string):
            print(f"{string} Length = {len(string)}")
        else:
            print(string, " String not valid. Length =", len(string))
            for letter in string.lower():
                if not is_valid(letter):
                    error_string += f"Error in the sequence {seqence} {letter.upper()} " \
                                    f"is not a valid nucleotide position " \
                                    f"{string.index(letter.upper())}\n"

    if error_string == "":
        print("All your sequences are valid")
        with open("error_report_fasta.txt", "w", encoding="utf-8") as error_report:
            error_report.write("ERROR REPORT\n")
            error_report.write(error_string)
    else:
        print("\nERROR REPORT")
        print(error_string)
        print("\nerror report generated")
        with open("error_report_fasta.txt", "w", encoding="utf-8") as error_report:
            error_report.write("ERROR REPORT\n")
            error_report.write(error_string)


def trimming_list(list_string, characters):
    """ Create a list of string and reformate from fasta file """
    list_string = list_string.split("\n")
    if len(characters) != 0 :
        for letter in characters:

            if letter in list_string:
                del list_string[list_string.index(letter)]

    return list_string
