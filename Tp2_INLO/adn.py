#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    A library for DNA string analyses
"""

__author__ = 'Michael Patron'


def purify(adn_str):
    """ Purify the DNA string """
    return adn_str


def is_valid(adn_str):
    """ Check is user input is a valid DNA string """
    nucleotidique_list = ["a", "t", "g", "c"]
    if adn_str is None or len(adn_str.replace(" ", "")) == 0:
        return False
    for letter in adn_str:
        if letter.lower() not in nucleotidique_list:
            return False
    return True


def get_valid_adn(prompt='string : '):
    """ DNA verification function. Use is_valid function """
    print("Check your DNA string:\n")
    user_string = input(prompt)
    while not is_valid(user_string):
        print("DNA string invalid")
        user_string = input("Enter a DNA string valid\nstring : ")
    print("Your DNA string is :", user_string.lower())
    return user_string
