#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Main execution program
"""

__author__ = 'Michael Patron'

from select_fasta_parser import main
from fasta import fasta_check


if __name__ == "__main__":
    fasta = main()
    fasta_check(fasta)
