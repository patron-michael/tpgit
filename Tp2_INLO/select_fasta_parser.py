#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    A library for bash execution with parser
"""

__author__ = 'Michael Patron'

import argparse
import sys


def create_parser():
    """ Declares new parser and adds parser arguments """
    program_description = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(add_help=True, description=program_description)
    parser.add_argument('-i', '--inputfile', default=sys.stdin,
                        help="required input file in fasta format", type=argparse.FileType("r"),
                        required=True)
    return parser


def main():
    """ Main function for reading fasta file and checking sequence format """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    #print(args["inputfile"]) display informations about the file
    fasta_sequence = args["inputfile"].read()
    return fasta_sequence
